﻿using UnityEngine;
using System.Collections;
#if UNITY_IPHONE

#else
using System.IO;
#endif
namespace XPlatSerializerLoaderSaver
{
    public static class XPlatFileIO
    {
        public static void WriteToDisk(string fileName, string data)
        {
            WriteWithSystemIO(fileName, data);
        }


        static void WriteWithSystemIO(string fileName, string data)
        {
            File.WriteAllText(Application.persistentDataPath + "\\" + fileName+".txt", data);
        }

        public static string Read(string fileName)
        {
            return File.ReadAllText(Application.persistentDataPath +string.Format("{0}{1}{2}", "\\", fileName, ".txt"));
        }
    }
}